<div align="center">
    <img src="./logo.png" alt="react-regions" />
</div>

<div align="center">
    <strong>Small Javascript package for managing regions on template using "regions" or "blocks". Idea is copied from "slots" of Vue.js.</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/LANG-TypeScript-%232b7489.svg?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/devmint/react-regions/master?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/coverage/devmint/react-regions/master?style=for-the-badge" />
</div>

<br />
<br />

Small Javascript package for managing regions on template using "regions" or "blocks". Idea is copied from "slots" of Vue.js.

## Installation

First you need to setup registry:

```bash
echo @devmint:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

or

```bash
echo \"@devmint:registry\" \"https://gitlab.com/api/v4/packages/npm/\" >> .yarnrc
```

Then you can install package using `npm`

```bash
npm i @devmint/react-regions
```

or `yarn`

```bash
yarn add @devmint/react-regions
```

## Usage

First, wrap your whole application with `RegionContext`. There are two ways to create context with initial value:

```tsx
import React from 'react'
import { regionContext, useRegionContextFactory } from '@devmint/react-regions'

const TestWrapper = () => {
  /**
   * Argument passed to `useRegionContextFactory` works similar to second argument
   * in `React.useEffect`.
   */
  const regionCtx = useRegionContextFactory(() => router.asPath)

  return (
    <regionContext.Provider value={regionCtx}>
      <YourAppComponent />
    </regionContext.Provider>
  )
}
```

With provided context for registering components into regions we need to create some layout system. Layout contains `<Region />` components
with predefined regions' names (e.g. `header`). Names might not be unique - the same component can be rendered into two different places.

```tsx
import React from 'react'
import { Region } from '@devmint/react-regions'

const YourAppComponent = () => (
  <>
    <Region name='header' />
    <Region name='main'>
      <Region name='content' />
      <Region name='sidebar' />
    </Region>
    <Region name='footer' />
  </>
)
```

With created `RegionContext` and defined layout system in `<YourAppComponent />` we can register component:

```tsx
import React from 'react'
import { useRegionSystem } from '@devmint/react-regions'

const HomePage = () => {
  useRegionSystem(ctx => {
    ctx.register('header', YourHeaderComponent)
    ctx.register('content', YourHomePageContent)
  })

  return null
}
```


## API

#### `RegionContext`

Using React's [`Context API`](https://reactjs.org/docs/context.html) we register components under some key (named _region's name_), unregister components 
and load them into `<Region />` component. It's some kind of _service locator_ pattern.

```ts
type RegionContext = {
  /**
   * Sometimes you want to refresh registered components (e.g. page transition).
   * To let know about it, you can define `refreshRegionOn` function:
   *
   * import { useRouter } from 'next/router'
   * const refreshRegionOn = () => useRouter().asPath
   */
  refreshRegionOn: () => unknown;

  /**
   * Looking for region in the registry.
   */
  find: (name: string) => React.ComponentType<unknown> | undefined;

  /**
   * Allows you to register the component into region. If another component
   * is registered, it overwrites the state. Only one component can be registered
   * in region.
   */
  register: <T = unknown>(
    name: string,
    region: React.ComponentType<T>,
    props?: React.PropsWithChildren<T>,
  ) => void;

  /**
   * Resets the state of registry. If argument `name` is passed, it removes
   * the region from registry.
   */
  reset: (name?: string) => void;
}
```

#### `<Region />`

Render component registered under some key in `RegionContext`.

```ts
type RegionProps = React.PropsWithChildren<
  {
    /**
     * Render component from `RegionContext` registered under this name.
     */
    name: string

    /**
     * Allows you to render component's wrapper with different HTML tag than <div />.
     */
    as?: keyof JSX.IntrinsicElements | React.ComponentType<RegionAsProps>
  } & Record<string, unknown>
>
```

#### `useRegionSystem()` hook

```tsx
/**
 * Hook for registering components into regions.
 * Example:
 *
 * useRegionSystem(ctx => {
 *   ctx.register('top--menu', YourComponent)
 * })
 */
export const useRegionSystem = useRegionSystemFactory(() => undefined)
```

#### `useRegionSystemFactory()`

```tsx
/**
 * Factory hook to creating your own `useRegionSystem` hooks. For example when
 * you have two layouts (e.g. HomeLayout and PostLayout) and you want to have
 * predefined components per layout:
 *
 * const useHomeLayout = useRegionSystemFactory(ctx => {
 *   ctx.register('top--logo', YourLogoComponent)
 * })
 *
 * `useHomeLayout` works the same as `useRegionSystem` but resets some components
 * to their default state (like `top--logo` in example above).
 */
export function useRegionSystemFactory(cb: (ctx: RegionContext) => void) {
  return function useRegionSystem(
    customCb: (ctx: RegionContext) => void,
    deps: React.DependencyList = [],
  ): void {
    const regions = useContext(regionContext)

    useEffect(() => {
      cb(regions)
      customCb(regions)
    }, [regions.refreshRegionOn(), ...deps])
  }
}
```