import * as React from 'react'
import { Region } from '../../src'
import { RegionProvider } from './simpleLayoutSystem'

type WithChildren<T = unknown> = T & { children?: React.ReactNode }
type PropAs = { as?: keyof JSX.IntrinsicElements }

export const LayoutWithAsProp = ({ as = 'div', children }: WithChildren<PropAs>): JSX.Element => {
  return (
    <RegionProvider>
      <Region as={as} name="dolor-a" />
      {children}
    </RegionProvider>
  )
}
