import * as React from 'react'
import styled from 'styled-components'
import { Region } from '../../src'
import { RegionProvider } from './simpleLayoutSystem'

type WithChildren<T = unknown> = T & { children?: React.ReactNode }

const AdvancedStyledRegion = styled(Region)`
  background-color: red;
`

export const AdvancedStyledSimpleLayout = ({ children }: WithChildren): JSX.Element => {
  return (
    <RegionProvider>
      <AdvancedStyledRegion name="dolor-a" forwardedAs="main" />
      {children}
    </RegionProvider>
  )
}
