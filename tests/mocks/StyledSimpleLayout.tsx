import * as React from 'react'
import styled from 'styled-components'
import { Region } from '../../src'
import { RegionProvider } from './simpleLayoutSystem'

type WithChildren<T = unknown> = T & { children?: React.ReactNode }

const StyledRegion = styled(Region)`
  background-color: red;
`

export const StyledSimpleLayout = ({ children }: WithChildren): JSX.Element => {
  return (
    <RegionProvider>
      <StyledRegion name="dolor-a" />
      {children}
    </RegionProvider>
  )
}
