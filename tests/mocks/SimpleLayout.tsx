import * as React from 'react'
import { Region } from '../../src'
import { RegionProvider } from './simpleLayoutSystem'

type WithChildren<T = unknown> = T & { children?: React.ReactNode }

export const ExampleComponentWithProps = ({ name }: { name: string }): JSX.Element => <p>{name}</p>
export const ExampleComponentAction = ({ onClick }: { onClick: () => void }): JSX.Element => (
  <a onClick={onClick}>Kliknij</a>
)

export const SimpleLayout = ({ children }: WithChildren): JSX.Element => {
  return (
    <RegionProvider>
      <Region name="dolor-a" />
      <Region name="lorem-b" />
      {children}
    </RegionProvider>
  )
}
