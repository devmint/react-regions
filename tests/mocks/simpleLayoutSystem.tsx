import * as React from 'react'
import { regionContext, RegionContext, useRegionContextFactory, useRegionSystem } from '../../src'

type WithChildren<T = unknown> = T & { children?: React.ReactNode }
type TestComponentProps = { className?: string }

export const simpleComponentFactory = (
  cb: (ctx: RegionContext) => void,
  innerText = 'Test Component',
  deps: React.DependencyList = [],
): React.ComponentType<TestComponentProps> => {
  const TestComponent = ({ className }: TestComponentProps) => {
    useRegionSystem(cb, deps)
    return <p className={className}>{innerText}</p>
  }

  return TestComponent
}

export const RegionProvider = ({
  children,
  onRefresh,
}: WithChildren<{ onRefresh?: () => void }>): JSX.Element => {
  const regionCtx = useRegionContextFactory(onRefresh || jest.fn())
  return <regionContext.Provider value={regionCtx}>{children}</regionContext.Provider>
}
