import * as React from 'react'
import { unassignRegionsFromCollection } from '../../src/hooks/useRegionContextFactory'

function HomeComponent() {
  return <p>Home Component</p>
}
function ContactComponent() {
  return <p>Contact Component</p>
}

function attachToState(
  key: string,
  component: React.ComponentType<Record<string, unknown>>,
): Record<
  string,
  { component: React.ComponentType<Record<string, unknown>>; props: Record<string, unknown> }
> {
  return { [key]: { component: component, props: {} } }
}

describe('Unassign region from collection', () => {
  it('should clear all regions when "name" argument is not passed', () => {
    const prevState = {
      ...attachToState('home', HomeComponent),
      ...attachToState('contact', ContactComponent),
    }
    const newState = unassignRegionsFromCollection()(prevState)

    expect(prevState).not.toBe(newState)
    expect(newState).toEqual({})
  })

  it('should remove only single region when "name" argument is passed', () => {
    const prevState = {
      ...attachToState('home', HomeComponent),
      ...attachToState('contact', ContactComponent),
    }
    const newState = unassignRegionsFromCollection('contact')(prevState)

    expect(prevState).toBe(newState)
    expect(newState).toHaveProperty('home')
  })
})
