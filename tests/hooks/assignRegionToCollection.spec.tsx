import * as React from 'react'
import { assignRegionToCollection } from '../../src/hooks/useRegionContextFactory'

function HomeComponent({}: { name?: string }) {
  return <p>Home Component</p>
}
function ContactComponent() {
  return <p>Contact Component</p>
}

describe('Assign region to collection only when component is different', () => {
  it('should change the state when both components are different', () => {
    const prevState = { home: { component: ContactComponent } }
    const newState = assignRegionToCollection('home', HomeComponent)(prevState)

    expect(prevState).not.toBe(newState)
    expect(newState.home.component).toBe(HomeComponent)
  })

  it('should change the state when props are different', () => {
    const prevState = { home: { component: ContactComponent } }
    const newState = assignRegionToCollection('home', ContactComponent, { name: 'A' })(prevState)

    expect(prevState).not.toBe(newState)
    expect(newState.home.component).toBe(ContactComponent)
  })

  it('should not change the state when both components are the same', () => {
    const prevState = { home: { component: ContactComponent } }
    const newState = assignRegionToCollection('home', ContactComponent)(prevState)

    expect(prevState).toBe(newState)
  })
})
