import * as React from 'react'
import { render, fireEvent } from '@testing-library/react'
import 'jest-styled-components'
import { simpleComponentFactory } from './mocks/simpleLayoutSystem'
import {
  ExampleComponentAction,
  ExampleComponentWithProps,
  SimpleLayout,
} from './mocks/SimpleLayout'
import { LayoutWithAsProp } from './mocks/LayoutWithAsProp'
import { StyledSimpleLayout } from './mocks/StyledSimpleLayout'
import { AdvancedStyledSimpleLayout } from './mocks/AdvancedStyledSimpleLayout'
import { useRegionSystem } from '../src'

describe('Render component by passing them named to Context API', () => {
  it('should render custom component in place of "dolor-a" region', () => {
    const TestComponent = simpleComponentFactory((ctx) => {
      ctx.register('dolor-a', () => <p>Dolor A Component</p>)
    })

    const { getByText } = render(
      <SimpleLayout>
        <TestComponent />
      </SimpleLayout>,
    )

    expect(() => getByText('Dolor A Component')).not.toThrow()
    expect(() => getByText('Test Component')).not.toThrow()
  })

  it('should render custom component in place of "lorem-b" region', () => {
    const TestComponent = simpleComponentFactory((ctx) => {
      ctx.register('lorem-b', () => <p>Dolor A Component</p>)
    })

    const { getByText } = render(
      <SimpleLayout>
        <TestComponent />
      </SimpleLayout>,
    )

    expect(() => getByText('Dolor A Component')).not.toThrow()
    expect(() => getByText('Test Component')).not.toThrow()
  })

  it('should render custom component in place of "lorem-b" and "dolor-a"', () => {
    const TestComponent = simpleComponentFactory((ctx) => {
      ctx.register('lorem-b', () => <p>Lorem B Component</p>)
      ctx.register('dolor-a', () => <p>Dolor A Component</p>)
    })

    const { getByText } = render(
      <SimpleLayout>
        <TestComponent />
      </SimpleLayout>,
    )

    expect(() => getByText('Dolor A Component')).not.toThrow()
    expect(() => getByText('Lorem B Component')).not.toThrow()
    expect(() => getByText('Test Component')).not.toThrow()
  })

  it('should render registered components only once', () => {
    const loremB = jest.fn(() => <p>Lorem B Component</p>)
    const TestComponent = simpleComponentFactory((ctx) => {
      ctx.register('lorem-b', loremB)
    })

    const { getByText } = render(
      <SimpleLayout>
        <TestComponent />
      </SimpleLayout>,
    )
    expect(() => getByText('Lorem B Component')).not.toThrow()
    expect(loremB).toHaveBeenCalledTimes(1)
  })

  describe('prop "as" allows to replace HTML element\'s tag', () => {
    const TestComponent = simpleComponentFactory((ctx) => {
      ctx.register('dolor-a', () => <>Text to find HTML element</>)
    })

    it('should render as "div" by default', () => {
      const { getByText } = render(
        <LayoutWithAsProp>
          <TestComponent />
        </LayoutWithAsProp>,
      )

      const htmlElement = getByText('Text to find HTML element')
      expect(htmlElement.tagName).toEqual('DIV')
    })

    it('should allow to render as "span"', () => {
      const { getByText } = render(
        <LayoutWithAsProp as={'span'}>
          <TestComponent />
        </LayoutWithAsProp>,
      )

      const htmlElement = getByText('Text to find HTML element')
      expect(htmlElement.tagName).toEqual('SPAN')
    })
  })

  describe('circular dependecy injection (see INTERNAL-10)', () => {
    it.skip('should prevent simple circular dependency injection', () => {
      const TestComponentB = simpleComponentFactory((ctx) => {
        ctx.register('dolor-a', TestComponentA)
      })
      const TestComponentA = simpleComponentFactory((ctx) => {
        ctx.register('dolor-a', TestComponentB)
      })

      render(
        <SimpleLayout>
          <TestComponentA />
        </SimpleLayout>,
      )
    })

    it('should prevent nested circular dependency injection', () => {
      const TestComponent = simpleComponentFactory((ctx) => {
        ctx.register('dolor-a', SimpleLayout)
        ctx.register('lorem-b', () => <p>Lorem B Component</p>)
      })

      render(
        <SimpleLayout>
          <TestComponent />
        </SimpleLayout>,
      )
    })
  })

  describe('with "styled-components" package (see INTERNAL-12)', () => {
    it('should render styled region with content', () => {
      const TestComponent = simpleComponentFactory((ctx) => {
        ctx.register('dolor-a', () => <p>Dolor A Component</p>)
      })

      const { container, getByText } = render(
        <StyledSimpleLayout>
          <TestComponent />
        </StyledSimpleLayout>,
      )

      expect(() => getByText('Dolor A Component')).not.toThrow()
      expect(container.firstChild).toMatchSnapshot()
    })

    it('should render styled region with content and "as" prop', () => {
      const TestComponent = simpleComponentFactory((ctx) => {
        ctx.register('dolor-a', () => <p>Dolor A Component</p>)
      })

      const { container, getByText } = render(
        <AdvancedStyledSimpleLayout>
          <TestComponent />
        </AdvancedStyledSimpleLayout>,
      )

      expect(() => getByText('Dolor A Component')).not.toThrow()
      expect(container.firstChild).toMatchSnapshot()
    })
  })

  describe("it's possible to pass props to provided component (see INTERNAL-13)", () => {
    type NameProps = { name?: string }
    type NameWithTProps = NameProps & { t?: number }

    it('should render value provided by prop', () => {
      const TestComponent = simpleComponentFactory((ctx) => {
        ctx.register('lorem-b', ({ name }: NameProps) => <p>{name}</p>, {
          name: 'Lorem ipsum',
        })
      })

      const { getByText } = render(
        <SimpleLayout>
          <TestComponent />
        </SimpleLayout>,
      )

      expect(() => getByText('Lorem ipsum')).not.toThrow()
    })

    it('should render an empty space when prop is not passed', () => {
      const TestComponent = simpleComponentFactory((ctx) => {
        ctx.register('lorem-b', ({ name = 'Empty name' }: NameProps) => <p>{name}</p>)
      })

      const { getByText } = render(
        <SimpleLayout>
          <TestComponent />
        </SimpleLayout>,
      )

      expect(() => getByText('Empty name')).not.toThrow()
    })

    it('should not overwrite existing default props of component', () => {
      const TestComponent = simpleComponentFactory((ctx) => {
        ctx.register(
          'lorem-b',
          ({ name = 'Empty name', t = 12 }: NameWithTProps) => <p>{`${name} - ${t}`}</p>,
          { name: 'ABCD' },
        )
      })

      const { getByText } = render(
        <SimpleLayout>
          <TestComponent />
        </SimpleLayout>,
      )

      expect(() => getByText('ABCD - 12')).not.toThrow()
    })

    it('should rerender regions when prop is changed (see INTERNAL-14)', () => {
      const TestComponent = () => {
        const [n, setN] = React.useState('lorem')
        useRegionSystem(
          (ctx) => {
            ctx.register('dolor-a', ExampleComponentWithProps, { name: n })
            ctx.register('lorem-b', ExampleComponentAction, { onClick: () => setN('dolor') })
          },
          [n],
        )
        return null
      }

      const { getByText } = render(
        <SimpleLayout>
          <TestComponent />
        </SimpleLayout>,
      )

      expect(() => getByText('lorem')).not.toThrow()
      fireEvent.click(getByText('Kliknij'))
      expect(() => getByText('dolor')).not.toThrow()
    })

    it('should not rerender regions when dependency list is empty (see INTERNAL-14)', () => {
      const TestComponent = () => {
        const [n, setN] = React.useState('lorem')
        useRegionSystem((ctx) => {
          ctx.register('dolor-a', ExampleComponentWithProps, { name: n })
          ctx.register('lorem-b', ExampleComponentAction, { onClick: () => setN('dolor') })
        })
        return null
      }

      const { getByText } = render(
        <SimpleLayout>
          <TestComponent />
        </SimpleLayout>,
      )

      expect(() => getByText('lorem')).not.toThrow()
      fireEvent.click(getByText('Kliknij'))
      expect(() => getByText('dolor')).toThrow()
    })
  })
})
