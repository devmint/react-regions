/**
 * COMPONENTS
 */
export { Region } from './components/Region'

/**
 * CONTEXTS
 */
export { regionContext } from './contexts/regionContext'

/**
 * HOOKS
 */
export { useRegionContextFactory } from './hooks/useRegionContextFactory'
export { useRegionSystem } from './hooks/useRegionSystem'
export { useRegionSystemFactory } from './hooks/useRegionSystemFactory'

/**
 * TYPES
 */
export type { RegionContext } from './contexts/regionContext'
