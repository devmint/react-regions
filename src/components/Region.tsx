import * as React from 'react'
import { regionContext } from '../contexts/regionContext'

type RegionAsProps = Record<string, unknown> & { id?: string }

type RegionProps = React.PropsWithChildren<
  {
    name: string
    as?: keyof JSX.IntrinsicElements | React.ComponentType<RegionAsProps>
  } & Record<string, unknown>
>

function PlainRegon({ as = 'div', children, name, ...props }: RegionProps) {
  return React.createElement(as, { ...props, id: name }, children)
}

const MemoizedRegion = React.memo(
  PlainRegon,
  ({ children: prevChildrenPure, ...prevProps }, { children: nextChildrenPure, ...nextProps }) => {
    const nameIsTheSame = prevProps.name === nextProps.name
    if (!nameIsTheSame) {
      return false
    }

    const prevChildren = React.isValidElement(prevChildrenPure)
      ? prevChildrenPure.type
      : prevChildrenPure
    const nextChildren = React.isValidElement(nextChildrenPure)
      ? nextChildrenPure.type
      : nextChildrenPure

    return Object.is(prevChildren, nextChildren) && Object.is(prevProps, nextProps)
  },
)

/**
 * <Region /> component register place in DOM layout where
 * the named region should be rendered. If any component is not
 * registered under the region's name then default content
 * is rendered (`children` prop).
 *
 * Example:
 * return (
 *   <Region name='logo-place'>
 *     <YourDefaultLogo />
 *   </Region>
 * )
 */
export const Region = React.memo((props: RegionProps) => {
  const regionCtx = React.useContext(regionContext)
  const Component = regionCtx.find(props.name)

  return <MemoizedRegion {...props}>{Component ? <Component /> : props.children}</MemoizedRegion>
})

Region.displayName = `MemoizedRegionWithContext`
MemoizedRegion.displayName = `MemoizedRegion`
