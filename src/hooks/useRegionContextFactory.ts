import * as React from 'react'
import { RegionContext } from '../contexts/regionContext'

type RegionsCollection = Record<
  string,
  {
    component: React.ComponentType<Record<string, unknown>>
    props?: Record<string, unknown>
  }
>

export function assignRegionToCollection<T = unknown>(
  name: string,
  region: React.ComponentType<React.PropsWithChildren<T>>,
  props?: React.PropsWithChildren<T>,
) {
  return function (prevRegions: RegionsCollection): RegionsCollection {
    const foundRegion = prevRegions[name]
    if (
      foundRegion &&
      Object.is(foundRegion.component, region) &&
      Object.is(foundRegion.props, props)
    ) {
      return prevRegions
    }

    return { ...prevRegions, [name]: { component: region, props } }
  }
}

export function unassignRegionsFromCollection(name?: string) {
  return function (prevRegions: RegionsCollection): RegionsCollection {
    if (name) {
      delete prevRegions[name]
      return prevRegions
    }
    return {}
  }
}

/**
 * Factory for default values for RegionContext.Provider.
 */
export function useRegionContextFactory(
  refreshRegionOn: RegionContext['refreshRegionOn'],
): RegionContext {
  const [regions, setRegion] = React.useState<RegionsCollection>({})

  const findRegion = React.useCallback(
    (name: string) => {
      const c = regions[name]
      if (!c) {
        return undefined
      }

      c.component.defaultProps = c.props
      return c.component
    },
    [regions],
  )

  const registerRegion = React.useCallback(
    <T = unknown>(
      name: string,
      region: React.ComponentType<React.PropsWithChildren<T>>,
      props?: React.PropsWithChildren<T>,
    ) => {
      return setRegion(assignRegionToCollection(name, region, props))
    },
    [setRegion],
  )

  const resetRegions = React.useCallback(
    (name?: string) => setRegion(unassignRegionsFromCollection(name)),
    [setRegion],
  )

  return {
    refreshRegionOn,
    find: findRegion,
    register: registerRegion,
    reset: resetRegions,
  }
}
