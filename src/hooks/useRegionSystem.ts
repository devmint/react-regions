import { useRegionSystemFactory } from './useRegionSystemFactory'

/**
 * Hook for registering components into regions.
 * Example:
 *
 * useRegionSystem(ctx => {
 *   ctx.register('top--menu', YourComponent)
 * })
 */
export const useRegionSystem = useRegionSystemFactory(() => undefined)
