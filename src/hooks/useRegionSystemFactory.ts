import { useContext, useEffect } from 'react'
import { RegionContext, regionContext } from '../contexts/regionContext'

/**
 * Factory hook to creating your own `useRegionSystem` hooks. For example when
 * you have two layouts (e.g. HomeLayout and PostLayout) and you want to have
 * predefined components per layout:
 *
 * const useHomeLayout = useRegionSystemFactory(ctx => {
 *   ctx.register('top--logo', YourLogoComponent)
 * })
 *
 * `useHomeLayout` works the same as `useRegionSystem` but resets some components
 * to their default state (like `top--logo` in example above).
 */
export function useRegionSystemFactory(cb: (ctx: RegionContext) => void) {
  return function useRegionSystem(
    customCb: (ctx: RegionContext) => void,
    deps: React.DependencyList = [],
  ): void {
    const regions = useContext(regionContext)

    useEffect(() => {
      cb(regions)
      customCb(regions)
    }, [regions.refreshRegionOn(), ...deps])
  }
}
