import * as React from 'react'

export type RegionContext = {
  /**
   * Sometimes you want to refresh registered components (e.g. page transition).
   * To let know about it, you can define `refreshRegionOn` function:
   *
   * import { useRouter } from 'next/router'
   * const refreshRegionOn = () => useRouter().asPath
   */
  refreshRegionOn: () => unknown

  /**
   * Looking for region in the registry.
   */
  find: (name: string) => React.ComponentType<unknown> | undefined

  /**
   * Allows you to register the component into region. If another component
   * is registered, it overwrites the state. Only one component can be registered
   * in region.
   */
  register: <T = unknown>(
    name: string,
    region: React.ComponentType<T>,
    props?: React.PropsWithChildren<T>,
  ) => void

  /**
   * Resets the state of registry. If argument `name` is passed, it removes
   * the region from registry.
   */
  reset: (name?: string) => void
}

export const regionContext = React.createContext<RegionContext>({
  refreshRegionOn: () => '',
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  find: (_name: string) => undefined,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  register: (_name: string, _region: React.ComponentType<unknown>): void => undefined,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  reset: (_name?: string) => undefined,
})
